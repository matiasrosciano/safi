var express = require('express');
var app = express();
var cors = require('cors')
const csv = require('csv-parser')
const fs = require('fs')
const redis = require('redis')

const redisclient = redis.createClient(6379)

const saveIavg_A = (device,metricid,start,end,data) =>{
  redisclient.set(`${device},${metricid},range`,`${start},${end}`)
  redisclient.set(`${device},${metricid}`,JSON.stringify(data))
}

const GetStatePercent = (data,fullCharge,start,end) =>{
  const StateOb = {Off:0,OnUnload:0,OnIdle:0,OnLoaded:0}
  let startindex = 0
  let endindex = data.length

  for(let i=0;i<data.length - 1;i++){
    if(start >= data[i].timestamp && start <= data[i+1].timestamp){
      startindex = i
      break
    }
  }
  for(let i=data.length-1;i>0;i--){
    if(end <= data[i].timestamp && end >=data[i-1].timestamp){
      endindex = i
      break
    } 
  }

  for(let i=startindex+1;i<endindex;i++){

    const gap = data[i].timestamp - data[i-1].timestamp
    const gapLimit = 30000
    if (gap > gapLimit) StateOb.Off += Math.floor(gap/gapLimit)
    let powerPercent = data[i].calcvalue * 100 / fullCharge
    if (powerPercent < 1){
      StateOb.OnUnload++
    }else if (powerPercent < 20){
      StateOb.OnIdle++
    }else{
      StateOb.OnLoaded++
    }
  }
  return StateOb
}

const getData = (req,res) =>{
  const { metricid, starttime, endtime, device } = req.query
  const powerdata = []
  let max = -Infinity

  
  fs.createReadStream('./../demoCompressorWeekData.csv')
    .pipe(csv())
    .on('data', (data) => {
        if (data.deviceid === device && data.metricid === metricid && data.timestamp >= starttime && data.timestamp <= endtime){
          powerdata.push({"timestamp":Number(data.timestamp), "calcvalue":Number(data.calcvalue)})
          if(Number(data.calcvalue)>max) max = Number(data.calcvalue)
        }
      }
    )
    .on('end', () => {
      saveIavg_A(device,metricid,starttime,endtime,{"data":powerdata,"fullCharge":max})
      let stateData = GetStatePercent(powerdata,max,starttime,endtime)
      res.send(stateData)
    });
}

const cache = (req,res,next) =>{
  const { metricid, starttime, endtime , device} = req.query
  redisclient.get(`${device},${metricid},range`, (err, data) => {
    if (err) throw err
    if (data !== null) {
      let range = data.split(',')
      if(starttime >= range[0] && endtime <= range[1]){
        redisclient.get(`${device},${metricid}`,(err,data)=>{
          if(err) throw err
          if(data !== null){
            let datajson = JSON.parse(data)
            res.send(GetStatePercent(datajson.data,datajson.fullCharge,starttime,endtime))
          }else{
            next()
          }
        })
      }else{
        next()
      }
    } else {
      next();
    }
  });
}


app.use(cors())
app.options('*',cors())


app.get('/metric',cache,getData);

app.listen(8000,()=> {
  console.log('Example app listening on port 8000!');
});
