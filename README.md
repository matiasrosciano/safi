# Safi Home Test

### The Problem

users want to know when a machine is on or off, loaded or unloaded. That will help them understand a) how utilized their equipment is, b) how long it takes to change tools, c) if the equipment is under-loaded, then how to improve the efficiency. 

The 4 states are: Off, On - unloaded, On - idle, On - loaded. 
Off is power off, no data. 
On - unloaded is power on, no current (or essentially no current, it could be 0.1A or something). 
On - idle is less than 20% of operating load. 
On - loaded is 20-100+% of operating load. 

### The challenge: 
Build a simple web interface that tracks and displays uptime vs downtime for this machine for a week. The interface should be written in whatever stack you want (node/vue/express isn't a requirement).

### The Solution:

I built a solution using Express, ReactJS, Redis.
this repo contains both client and server.

Must be run
#### `npm install`

And
#### `npm start`
in both directory client and server

A local server should start at http://localhost: with port:3000 for client and :8000 for server

# Important Note
## Redis is a dependency for this solution, redis must be running locally on port 6379

### How use

On the home page the user finds the name of the device and two inputs "From and To" this set the request,when the user selects the dates and press the button "GetData" the client send a request, when server responds, a Graph is rendered with the percentages of the time the device was in each state on selected dates.
The user can change the dates, but only within the dates of the csv file.

### Under the hood

When the first request is sent, the server parses the csv file, filtering by device(in this case only 1). Dates, And Metricid. save the filtered data and filters in Redis.
when other requests sent, the system uses the filters saved in redis to know if the new filters sent in the new request are within the set of the first, if this happens, the system use the data saved in redis. Processes that and return the new data, faster than if I had to parse the file again

### List for improvements

- Limits Dates, These are set in the front, it would be convenient that when starting the app these are set according to the limits of the csv file
- Add a selection input with the device name, In this case the csv file contains info of only one device.
- File uploader, server side rendering.
- Show dates where a state like off or under-load a for long time