import {API_URL} from './../constants/api'

const getPowerData = async (from,to) =>{
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'}
    }
    try{
        let response = await fetch(`${API_URL}/metric?device=demo_ca1_t_axm&metricid=Iavg_A&starttime=${from}&endtime=${to}`,requestOptions)
        let data = await response.json()
        return data
    } catch(e){
        alert(e)
    }
}

export const devicesServices = {
    getPowerData
}