import React from 'react';
import PowerData from './components/PowerData'


function App() {

  return (
    <div className="App" style={{display:'flex',flexDirection:'column',alignItems:'center'}} >
      <h1>Safi</h1>
      <PowerData/>
    </div>
  );
}

export default App;
