import React,{useState} from 'react'
import '../../node_modules/react-vis/dist/style.css';
import {XYPlot, VerticalBarSeries, XAxis, YAxis,VerticalGridLines,HorizontalGridLines,LabelSeries} from 'react-vis';
import { devicesServices } from '../services/devicesServices';


const getLocalDates = (timestamp) =>{
    return (new Date(new Date(timestamp).toString().split('GMT')[0]+' UTC').toISOString().split('.')[0])
}
const Limits = {
    startdate: getLocalDates(1534723200000),
    enddate: getLocalDates(1535120055000)
}

const PowerData = props =>{

    const [inputs,setInputs] = useState({
        startdate:Limits.startdate,
        enddate:Limits.enddate
    })
    const [data,setData] = useState()
    const [sended,setSended] = useState(false)

    const validvalue = ({value}) =>{
        let date = new Date(value)
        let min = new Date(1534723200000)
        let max = new Date(1535120055000)
        return ( date > min && date < max )
    }
    
    const handleOnChange = e =>{
        if (validvalue(e.target)){
            let newState = {...inputs}
            newState[e.target.name] = e.target.value
            setInputs(newState)
        }else{
            alert('Invalid Input')
        }
    }

    const handleOnSubmit = () =>{
        setData(null)
        const {startdate,enddate} = inputs
        const requestStart = (new Date(startdate)).getTime()
        const requestEnd = (new Date(enddate)).getTime()
        if (requestStart < requestEnd){
            setSended(true)
            devicesServices.getPowerData(requestStart,requestEnd)
                .then(data=>setData(data))
                .catch(err=>alert(err))
        }else{
            setSended(false)
            alert("'From date' can't be after than 'To date'")
        }
    }

    const RenderData = () =>{
        const GraphData = []
        const total = Object.values(data).reduce((sum,num)=>sum + num)
        const values = Object.entries(data)

        values.forEach(value=>{
            let auxob = {"x":value[0],"y":value[1] * 100 / total,"label": (value[1] * 100 / total).toFixed(4) + '%',xOffset: 30}
            GraphData.push(auxob)
        })
        return(
          <XYPlot height={500} width={600} xType="ordinal" yDomain={[0, 100]}>
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis/>
                <YAxis/>
                <VerticalBarSeries
                    color='#f00'
                    data={GraphData}
                />
                <LabelSeries
                    data={GraphData}
                    allowOffsetToBeReversed
                    xOffset='10'
                />
          </XYPlot>
        )
      } 

    return(
        <div>
            <h2 style={styles.deviceid}>Device id: demo_ca1_t_axm</h2>
            <div style={styles.inputsbox}>
            <label htmlFor="startdate" style={styles.label}> From: </label>
            <input type="datetime-local" name="startdate" id="startdate" min={Limits.startdate} max={Limits.enddate} value={inputs.startdate} onChange={handleOnChange}  style={styles.input}/>
            <label htmlFor="enddate"  style={styles.label}> To: </label>
            <input type="datetime-local" name="enddate" id="end" min={Limits.startdate} max={Limits.enddate} value={inputs.enddate} onChange={handleOnChange} style={styles.input}/>
            <input type="button" value="Get Data" onClick={handleOnSubmit} style={styles.button}/>
            </div>
            {
                sended?
                    data?
                        RenderData()
                    :
                        <div>Loanding...</div>
                :
                    null
            }
        </div>
    )

}

const styles = {
    inputsbox:{
        display:'flex',
        justifyContent:'center'
    },
    deviceid:{
        textAlign:'center'
    },
    label:{
        marginRight:'0.5em'
    },
    input:{
        marginRight:'1em'
    },
    button:{
        boxShadow:'inset 0px 1px 0px 0px #f29c93',
        background:'linear-gradient(to bottom, #fe1a00 5%, #ce0100 100%)',
        backgroundColor:'#fe1a00',
        borderRadius:'16px',
        border:'1px solid #d83526',
        display:'inline-block',
        cursor:'pointer',
        color:'#ffffff',
        fontWeight:'bold',
        padding:'0px 12px',
        textDecoration:'none',
        textShadow:'0px 1px 0px #b23e35'
    }
}

export default PowerData
